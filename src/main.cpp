#include <stdio.h>
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include "game.h"

int main(int argc, char** args) {
    SDL_Log("hello world\n");

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {

        SDL_Log("error init\n");
        exit(1);
    }

    SDL_Window* window = SDL_CreateWindow("king", 100, 100, 1024, 768, SDL_WINDOW_OPENGL);
    SDL_GLContext context = SDL_GL_CreateContext(window);

    GLenum err = glewInit();
    if (GLEW_OK != err) {
        SDL_Log("glew error, bailing out! %s", glewGetErrorString(err));
    }

    SDL_Log("GL version: %s\n", glGetString(GL_VERSION));

    Game game("king2020");


    bool run = true;
    while (run) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                run = false;    
            }
        }

        glClearColor(0, 1, 0.5, 1);
        glClear(GL_COLOR_BUFFER_BIT);
        SDL_GL_SwapWindow(window);

    }

    SDL_DestroyWindow(window);
    SDL_GL_DeleteContext(context);
	return 0;
}
